-- ================================================
-- @Copyright Vipul Medcare Pvt. Ltd. 2017-18
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Usp_ECard
(@mode varchar(100)
,@CardId int=null
,@UserName varchar(100)=null
,@CardNumber varchar(16)=null
,@UserId int=null
,@ReportType  varchar(100)=null
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if @mode='ECard'--ECard
	begin
	BEGIN TRANSACTION SERIALIZABLE
    BEGIN TRY

	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		Select  @CardId=Dbo.GetCardId(@CardNumber) 
		if(@CardId=0)
		begin
		select 'Invalid Card';
		ROLLBACK;
		return;
		end

		declare @dependentId int;
		select @dependentId = Id  from  UserDetails  where RefLoginId=@UserId

		SELECT CrdMst.CardNumber
		,CrdType.CardName
		,Replace((UD.FirstName + ' ' + isnull(UD.MidName, '') + ' ' + isnull(UD.LastName, '')), '  ', ' ') AS CardHolderName
		,case when CrdMst.RefLoginId=@UserId then 'M' else 'D' end   [UserType]
		,(RIGHT('0' + RTRIM(MONTH(CrdMst.CretatedDate)), 2) + '/' + RIGHT(YEAR(CrdMst.CretatedDate), 2)) AS [ValidFrom]
		,(RIGHT('0' + RTRIM(MONTH(dateadd(month, 0, CrdMst.CretatedDate))), 2) + '/' + RIGHT(YEAR(dateadd(year, 1, CrdMst.CretatedDate)), 2)) AS [ValidTill]
		,(
		CASE 
		WHEN CrdType.Id = 1
		THEN 'H3UImages/Card/Silver.png'
		WHEN CrdType.Id = 2
		THEN 'H3UImages/Card/Gold.png'
		WHEN CrdType.Id = 3
		THEN 'H3UImages/Card/Diamond.png'
		WHEN CrdType.Id = 4
		THEN 'H3UImages/Card/Platinum.png'
		ELSE 'H3UImages/Card/NotFound.png'
		END
		) AS CardIcon
		FROM CardMaster AS CrdMst
		JOIN CardType AS CrdType ON CrdType.Id = CrdMst.RefCardTypeId
		join UserDetails as UD on UD.Id=@dependentId
		WHERE CrdMst.Id = @CardId
		AND isnull(CrdMst.IsActive, 0) = 1


   COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		exec usp_InserErrorLog
			SELECT 'Please contact to administrator' AS [Message]
		END	CATCH
	end
END
GO
