-- ================================================
-- @Copyright Vipul Medcare Pvt. Ltd.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE Usp_Profile
(@mode varchar(100)
,@UserName varchar(100)=null
,@UserId int=null
,@LoginTypeId int =null
,@FirstName varchar(100)=null
,@MidName varchar(100)=null
,@LastName varchar(100)=null
,@MobileNo varchar(10)=null
,@EmailId varchar(100)=null
,@Dob date=null
,@PermaAdd varchar(200)=null
,@BloodGroup varchar(5)=null
,@Gender int = null
,@ProfileImgCustom nvarchar(100)=null
)
AS
BEGIN
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	if(@mode='Profile')--Profile
	begin

	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		SELECT
			 Replace(ISNULL(UD.FirstName, '')+ ' '+ ISNULL(UD.MidName, '')+ ' '+isnull(UD.LastName, ''),'  ','') as [Name]
			,ISNULL(UD.FirstName, '') as FirstName
			,ISNULL(UD.MidName, '') as MiddleName
			,ISNULL(UD.LastName, '') as LastName
			,(ISNULL(Ud.MobileNo, '')) AS Mobile
			,ISNULL(UD.EmailId, '') AS EmailId
			,REPLACE(CONVERT(VARCHAR(12), UD.DOB, 106), ' ', '-') AS DOB
			,CONVERT(VARCHAR(10), (
					CASE 
						WHEN ISNULL(UD.DOB, '') != ''
							THEN cast(DATEDIFF(hour, UD.DOB, GETDATE()) / 8766.0 AS INT)
						ELSE isnull(UD.Age,'')
						END
					)) Age
			,isnull(UD.PermaAdd,'') AS [Address]
			,UD.BloodGroup AS [BloodGroup]
			,(
				CASE 
					WHEN UD.Gender = 1	THEN 'Male'
					WHEN UD.Gender = 2	THEN 'Female'
					WHEN ISNULL(UD.Gender, '') = ''	then ''  else '' END
				) AS [Gender]
			,case when isnull(MBOTP.Id,'')='' then 'false' else 'true' end IsMobVerified
			,'false' AS IsEmailVerified
			, (case when @LoginTypeId=1 then UD.ProfileImgCustom
		        when @LoginTypeId=2 then SU.ProfileImgFb
				when @LoginTypeId=3 then SU.ProfileImgGoogle else '' end) [ProfileImage]
		FROM LoginAccount AS SU
		Join UserDetails as UD on UD.RefLoginId = SU.Id
		left join Gender as GD on GD.Id = isnull(UD.Gender,0)
		left join MobileOTP as MBOTP on MBOTP.RefLoginId=@UserId
		WHERE SU.Id = @UserId
	end

    if(@mode='ProfileUpdate')
	begin
	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		UPDATE UserDetails 	SET  FirstName= (Case when  isnull(@FirstName,'')='' then FirstName else @FirstName end)
		                        ,MidName= (Case when  isnull(@MidName,'')='' then MidName else @MidName end)
								,LastName= (Case when  isnull(@LastName,'')='' then LastName else @LastName end)
								,MobileNo= (Case when  isnull(@MobileNo,'')='' then MobileNo else @MobileNo end)
								,DOB= (Case when  isnull(@Dob,'')='' then Dob else @Dob end)
								,PermaAdd= (Case when  isnull(@PermaAdd,'')='' then PermaAdd else @PermaAdd end)
								,BloodGroup= (Case when  isnull(@BloodGroup,'')='' then BloodGroup else @BloodGroup end)
								,Gender= (Case when  isnull(@Gender,'')='' then Gender else @Gender end)
		                        ,ProfileImgCustom = (case when isnull(@LoginTypeId,0)=0 then (case when isnull(@ProfileImgCustom,'')='' then @ProfileImgCustom else ProfileImgCustom end) else ProfileImgCustom end)
		WHERE RefLoginId = @UserId
		IF @@ROWCOUNT > 0
		BEGIN
			SELECT 'Profile update successfully' AS [SMessage]
		END
		ELSE
		BEGIN
			SELECT 'Some error occurred while update profile' AS [Message]
		END
	end
END
GO
