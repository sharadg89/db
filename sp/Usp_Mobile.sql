-- ================================================
-- @CopyRight Vipul MedCare Pvt. Ltd.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 08-May-2017
-- Description:	Mobile OTP
-- =============================================
CREATE PROCEDURE Usp_Mobile 
(@mode varchar(100)
,@UserName varchar(100)=''
,@Otp varchar(6)=''
,@Id int = null
,@Mobile varchar(12)=null
)
AS
BEGIN

		SET NOCOUNT ON;
		if @mode ='MobileOtp'
		begin
	
		select Id=@Id  FROM LoginAccount WHERE UserName = @UserName


		IF (isnull(@Id,'')='')
		BEGIN
		SELECT 'Invalid User' AS [Message]
		return;
		END

		IF ( isnull(@Otp,'')='')
		BEGIN

		DECLARE @RandomOTP INT;
		DECLARE @Upper INT;
		DECLARE @Lower INT;

		SET @Lower = 100000
		SET @Upper = 999999
		SET @RandomOTP = ROUND(((@Upper - @Lower - 1) * RAND() + @Lower), 0)

		INSERT INTO MobileOTP (
		RefLoginId
		,MobileNo
		,[OTP]
		)
		VALUES (
		@Id
		,@Mobile
		,@RandomOTP
		)

		SELECT 'false' AS [isOTPVerified],@RandomOTP AS [OTP]
		END

		Else
		begin

		IF EXISTS (
		SELECT id
		FROM [MobileOTP]
		WHERE isnull(IsActive,0) = 0
		AND MobileNo = @Mobile
		AND RefLoginId = @Id
		and OTP=@Otp
		)
		begin
		update [MobileOTP]  set  isActive = 1  where  MobileNo = @Mobile	AND RefLoginId = @Id 
		SELECT 'true' AS [isOTPVerified],'Valid OTP' AS [OTP]
		end
		else
		begin
		SELECT 'false' AS [isOTPVerified],'Invalid OTP' AS [OTP]
		RETURN;
		end

		END

	END
END








