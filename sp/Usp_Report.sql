-- ================================================
-- @Copyright Vipul Medcare Pvt. Ltd.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE Usp_Report
(@mode varchar(100)
,@CardId int=null
,@UserName varchar(100)=null
,@CardNumber varchar(16)=null
,@UserId int=null
,@ReportType  varchar(100)=null
)
AS
BEGIN
	
	SET NOCOUNT ON;
    if(@mode='Report')--TotalReport
	begin

	    BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY

	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		IF (@ReportType = 'HealthPack')
		BEGIN
		SELECT DISTINCT HealthChk.Id AS [HealthCheckID]
		,Replace((isnull(UDD.FirstName, '') + ' ' + isnull(UDD.MidName, '') + ' ' + ISNULL(UDD.LastName, '')), '  ', '') [PatientName] --NAME
		,(case when isnull(RM.Id,'') ='' then 'Self' else  RM.Relation end)  RelationShip
		,CD.CardNumber
		,REPLACE(CONVERT(VARCHAR(12), HealthChk.AppoinmentDateTime, 106), ' ', '-') AS CheckUpDate
		FROM HealthCheckupDtl AS HealthChk
		JOIN UserDetails UDD ON UDD.Id = HealthChk.RefDependentId 
		Join CardMaster as CD on CD.ID=HealthChk.RefCardId
		left join UserDependentDetails as DLnk on DLnk.RefDependentId=UDD.Id
		left join Relations as RM on RM.ID=DLnk.RefRelationId
		WHERE UDD.RefLoginId=@UserId
		END

		IF (@ReportType = 'Opd')--OPD
		BEGIN
		SELECT DISTINCT CardTrnsMst.TransactionID
		,Replace((isnull(UDD.FirstName, '') + ' ' + isnull(UDD.MidName, '') + ' ' + ISNULL(UDD.LastName, '')), '  ', '') [Name]
		,(case when isnull(RM.Id,'') ='' then 'Self' else  RM.Relation end)  RelationShip
		,CardTrnsMst.CardNo
		,CardTrnsMst.CouponCode
		,(
		CASE 
		WHEN CardTrnsMst.chk_status = 'Closed'
		THEN 'Coupon Used'
		WHEN CardTrnsMst.chk_status IN (
		'Pending'
		,'Approved'
		)
		AND datediff(HOUR, (CGen.InsertedOn), getdate()) > 10
		THEN 'Expired'
		ELSE 'Active'
		END
		) AS STATUS
		FROM MHC..UserCardTransactionDtl AS CardTrnsMst
		JOIN CardCoupon AS CGen ON CGen.CouponCode = CardTrnsMst.CouponCode
		JOIN UserDetails AS UDD ON UDD.Id = CGen.InsertedFrom
		left join UserDependentDetails as DLnk on DLnk.RefDependentId=UDD.Id
		left join Relations as RM on RM.ID=DLnk.RefRelationId
		WHERE UDD.Id=@UserId
		AND CardTrnsMst.BookType = 'Coupon USE'
		END


		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		exec usp_InserErrorLog
			SELECT 'Please contact to administrator' AS [Message]
		END	CATCH

	end
END
GO
