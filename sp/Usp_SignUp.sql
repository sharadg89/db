USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_SignUp]    Script Date: 10-05-2017 16:43:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Usp_SignUp] (
 @mode varchar(100)
,@USER_ID int =''
,@Password  varchar(16)=''
,@FName varchar(100)=''
,@MName varchar(100)=''
,@LName varchar(100)=''
,@UserName varchar(100)=''
,@SocialId	Varchar(50)=''
,@LoginType int = ''
,@columnname  varchar(100)=''
,@RefLoginId int =''
,@SocialImg varchar(200)=''
,@GoogleId	Varchar(100)=''
,@FbId	Varchar(50)=''
,@ProfileImgGoogle Varchar(200)=''
,@ProfileImgFb Varchar(200)=''
)
as
begin
IF @mode = 'SignUp'
	BEGIN

	    select @columnname = case when @LoginType=1 then 'username'
		                          when @LoginType=2 then 'FbId'
								  when @LoginType=3 then 'GoogleId'
								  else 'NoColumnfound' end
					
		IF  EXISTS (
				SELECT Id
				FROM [dbo].[LoginAccount]
				WHERE (
						@LoginType = 1
						and UserName=@UserName )
						OR (
						@LoginType = 2
						and FbId=@SocialId)
						OR 
						(@LoginType = 3
						AND GoogleId= @SocialId)
					  )
		BEGIN
		 exec Usp_VerifyLogin '',@UserName,@Password,'VerifyLogin','',@LoginType,@SocialId,''
		return;
		END

		

		BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY
		                declare @Query nvarchar(4000);
		                if(@columnname='FbId')
						begin
						set @FbId=@SocialId
						set @ProfileImgFb=@SocialImg
						end
						if(@columnname='GoogleId')
						begin
						set @GoogleId=@SocialId
						set @ProfileImgGoogle=@SocialImg
						end
		
						if exists(SELECT Id	FROM [dbo].[LoginAccount] WHERE UserName=@UserName)
						begin
						set @Query='';
						set @Query=
						'update LoginAccount set '+ @columnname + '='''+@SocialId+''',ProfileImgFb=''' + @ProfileImgFb + ''',ProfileImgGoogle= ''' + @ProfileImgGoogle + ''' 
						where userName= ''' + @UserName + ''''
						exec  (@Query)
						end
						else
						begin
                        INSERT INTO [dbo].[LoginAccount]([UserName],[Pwd],[UserType],[FbId],[GoogleId],ProfileImgFb,ProfileImgGoogle)
						VALUES(@UserName,@Password,'U',@FbId,@GoogleId,@ProfileImgFb,@ProfileImgGoogle)
			            SET @USER_ID = @@IDENTITY;
                        INSERT INTO [dbo].[UserDetails]([RefLoginID],[FirstName],[MidName],[LastName],EmailId)
                        VALUES(@USER_ID,dbo.fn_title_case(@FName),dbo.fn_title_case(@MName),dbo.fn_title_case(@LName),@UserName)
						end
						IF @@ROWCOUNT > 0
						BEGIN
						exec Usp_VerifyLogin '',@UserName,@Password,'VerifyLogin','',@LoginType,@SocialId,''
						END
						ELSE
						BEGIN
						SELECT 'User Registeration Failed' AS [Message]
						ROLLBACK TRANSACTION
						RETURN;
						END

		COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			SELECT 'User Registeration Failed' AS [Message]
			RETURN;
			ROLLBACK TRANSACTION
		END CATCH

		END    --Mode End for SignUp   
END