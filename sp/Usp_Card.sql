USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_Card]    Script Date: 11-05-2017 14:01:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 06-May-2017
-- Description:	Card  Related Info
-- =============================================
ALTER PROCEDURE [dbo].[Usp_Card]
(
 @mode varchar(100)
,@Id int =null
,@UserId int =null
,@UserName varchar(100)=null
,@CardId int =null
,@CardNumber varchar(16)=''
)
AS
BEGIN
	
	SET NOCOUNT ON;

	--Basic Info CardType
    if @mode ='CardType'
	begin
	SELECT   Id
			,CardName
			,Price
			,OpdCoupon AS [Free OPD's]
			,FMO AS [First Medical Opinion]
			,SMO AS [Second Medical Opinion]
			,BasicCheckup AS [Basic Health Checkups]
			,(
				CASE 
					WHEN CT.Id = 1
						THEN 'H3UImages/Card/Silver.png'
					WHEN CT.Id = 2
						THEN 'H3UImages/Card/Gold.png'
					WHEN CT.Id = 3
						THEN 'H3UImages/Card/Diamond.png'
					WHEN CT.Id = 4
						THEN 'H3UImages/Card/Platinum.png'
					ELSE 'H3UImages/Card/NotFound.png'
					END
				) AS CardImg
		FROM CardType AS CT WITH (NOLOCK)
		WHERE ISNULL(IsActive, 0) = 1
		ORDER BY Id 


		SELECT 'H3UImages/Icons/OPD.png' OPDImage,
		       'H3UImages/Icons/HealthChckup.png' FMOImage,
			   'H3UImages/Icons/fmo.jpg' SMOimage,
			   'H3UImages/Icons/smo.jpg' BasicCheckup

	END--End Basic Info CardType

	IF @mode = 'CardDtl'
	BEGIN
		IF EXISTS (
				SELECT Id
				FROM CardType WITH (NOLOCK)
				WHERE ISNULL(isactive, 0) = 1
					AND Id = @Id
				)
		BEGIN
			SELECT
			     Id
				,CardName
				,Price
				,OpdCoupon AS [Free OPD's]
				,FMO AS [First Medical Opinion]
				,SMO AS [Second Medical Opinion]
				,BasicCheckup AS [Basic Health Checkups]
				,(
					CASE 
						WHEN CT.CardTypeCode = '1'
							THEN 'H3UImages/Card/Silver.png'
						WHEN CT.CardTypeCode = '2'
							THEN 'H3UImages/Card/Gold.png'
						WHEN CT.CardTypeCode = '3'
							THEN 'H3UImages/Card/Diamond.png'
						WHEN CT.CardTypeCode = '4'
							THEN 'H3UImages/Card/Platinum.png'
						ELSE 'H3UImages/Card/NotFound.png'
						END
					) AS CardImg
			FROM mhc..[tbl_CardType] AS CT WITH (NOLOCK)
			WHERE ISNULL(isactive, 0) = 1
				AND Id = @Id
			ORDER BY Id
			
			
			SELECT  
					'H3UImages/Icons/OPD.png' OPDImage
					,'H3UImages/Icons/fmo.jpg' FMOImage
					,'H3UImages/Icons/smo.jpg' SMOimage
					,'H3UImages/Icons/HealthChckup.png' BasicCheckup

              SELECT [Id],[Feature],[RefCardTypeId]  FROM CardFeature
              WHERE Id = @Id AND isnull(IsActive,0) = 1 
		END
		ELSE
		BEGIN
			SELECT 'Invalid Card Id' AS [Message]
		END
	END

	IF @mode = 'UserWiseCardNo'
	BEGIN
	    
		Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		return;
		end

		declare @dependentId int;
		select @dependentId = Id  from  UserDetails  where RefLoginId=@UserId
		select  CDM.CardNumber,
		Replace((UD.FirstName + ' ' + isnull(UD.MidName, '') + ' ' + isnull(UD.LastName, '')), '  ', ' ') AS CardHolderName,
		case when CDM.RefLoginId=@UserId then 'M' else 'D' end   DependentType
		,CT.CardName
		,(
			CASE 
			WHEN CT.Id = '1'
			THEN 'H3UImages/Card/Silver.png'
			WHEN CT.Id = '2'
			THEN 'H3UImages/Card/Gold.png'
			WHEN CT.Id = '3'
			THEN 'Card/Diamond.png'
			WHEN CT.Id = '4'
			THEN 'H3UImages/Card/Platinum.png'
			ELSE 'H3UImages/Card/NotFound.png'
			END
		 ) AS CardImg
		,case when isnull(CT.corpid,'')='' then '' else corp.imagepath end CorpLogo
		,(RIGHT('0' + RTRIM(MONTH(CDM.CretatedDate)), 2) + '/' + RIGHT(YEAR(CDM.CretatedDate), 2)) AS [PurchseDate]
		,(RIGHT('0' + RTRIM(MONTH(DATEADD(yy, 1, CDM.CretatedDate))), 2) + '/' + RIGHT(YEAR(DATEADD(yy, 1, CDM.CretatedDate)), 2)) AS [ExpirDate]
		,'H3UImages/Icons/' + 'OPD.png' AS OPDImg
		,isnull(CT.OpdCoupon, 0) AS OPDValue
		,'H3UImages/Icons/' + 'HealthChckup.png' AS HlthChkImg
		,isnull(CT.BasicCheckup, 0) AS NoofHealthChkup
		,'H3UAPI/H3UImages/Icons/' + 'fmo.jpg' AS FmoImg
		,isnull(CT.FMO, 0) AS NoofFMO
		,'H3UImages/Icons/' + 'smo.jpg' AS SmoImg
		,ISNULL(CT.SMO, 0) AS NoofSMO
		,'H3UImages/H3ULogo/H3ULogo.png' AS [H3ULogo]
		,Replace(convert(varchar(12),CDM.CretatedDate,106),' ','-') as CreatedDate
		from CardMaster as CDM
		left join UserCardMapping as UCDM on  UCDM.RefDependentId=@dependentId
		JOIN CardType CT ON CT.Id = case when (case when CDM.RefLoginId=@UserId then 'M' else 'D' end)='M' then CDM.RefCardTypeId  else UCDM.RefCardTypeId end
		left join h3u..h3u_config_mstcorporate as corp on corp.corpid=CT.corpid
		join UserDetails as UD on UD.RefLoginId=@UserId  
	END

	If @mode='CardNoWithValue'
	begin
	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		return;
		end


		DECLARE @HealthCheckId INT;
		DECLARE @UsedOpd INT;
		DECLARE @ActiveCoupon INT;

		select  @CardId=Id  from   CardMaster where CardNumber=@CardNumber
		SELECT @HealthCheckId = COUNT(Id)
		FROM HealthCheckupDtl
		WHERE RefCardId = @CardId

		SELECT @UsedOpd = COUNT(TransactionID)
		FROM MHC..UserCardTransactionDtl
		WHERE CardNo = @CardNumber                                  
		AND ISNULL(Active, 0) = 1
		AND ((UPPER(chk_status) = 'CLOSED'))
		AND BookType = 'Coupon USE'


		SELECT @activecoupon = COUNT(CGen.id)
		FROM CardCoupon AS CGen
		join CardMaster as CDM on CDM.Id=CGen.RefCardId
		WHERE CDM.CardNumber = @CardNumber
		AND isnull(CGen.isactive, 0) = 1
		AND datediff(HOUR, (CGen.InsertedOn), getdate()) < 10
		AND CGen.CouponCode NOT IN (
		SELECT CouponCode
		FROM MHC..UserCardTransactionDtl
		WHERE cardno = @CardNumber
		AND ((UPPER(chk_status) = 'CLOSED'))
		)
		AND 
		dateadd(hour,1,CGen.InsertedOn)>=  GETDATE()
			



		DECLARE @opdAveleble1 INT;
		SELECT @opdAveleble1 = (CARDTYPE.OpdCoupon - (@activecoupon + @usedopd))
		FROM  CardMaster AS CARDMST
		JOIN  CardType AS CARDTYPE ON CARDTYPE.Id = CARDMST.RefCardTypeId
		WHERE CARDMST.CardNumber = @CardNumber 

		
		IF (@opdAveleble1 < 0)
		BEGIN
			SET @opdAveleble1 = 0;
			SELECT 'No available OPD coupon' AS [Message]
			RETURN;
		END

		DECLARE @THC INT;
		DECLARE @UHC INT;
		DECLARE @AHC INT;
		DECLARE @ACHC INT;

		SELECT @THC = CONVERT(INT, CARDTYPE.BasicCheckup)
			,@UHC = @HealthCheckID
			,@AHC = (CARDTYPE.BasicCheckup - @HealthCheckId)
		FROM  CardMaster AS CARDMST
		JOIN CardType AS CARDTYPE ON CARDTYPE.Id = CARDMST.RefCardTypeId
		WHERE CARDMST.CardNumber = @CardNumber 

		SET @ACHC = @THC - (@UHC + @AHC)

		SELECT (CARDTYPE.OpdCoupon - (@activecoupon + @usedopd)) AS [OPDavailable]
			,@activecoupon AS ActiveCoupon
			,@usedopd AS UsedCoupon  
			,(CARDTYPE.BasicCheckup - @HealthCheckID) AS AvailableHealthCheckup
			,'0' AS UsedHealthCheckup -- @HealthCheckID AS UsedHealthCheckup
			,CARDTYPE.CardName
			,'6' AS [TotalMembersAllowed]
			,CARDTYPE.OpdCoupon AS [TotalOPDs]
			,CONVERT(INT, CARDTYPE.BasicCheckup) AS [TotalHealthCheckUP]
			,@UHC AS [ActiveHealthCheckUP] 
		FROM CardMaster AS CARDMST
		JOIN CardType AS CARDTYPE ON CARDTYPE.Id = CARDMST.RefCardTypeId
		WHERE CARDMST.CardNumber = @CardNumber


	end
	
END

