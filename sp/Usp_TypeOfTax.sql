-- ================================================
-- @Copyright Vipul Medcorp Pvt. Ltd.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Usp_TypeOfTax
(@mode varchar(100)
,@CardTypeId int=null)
as
BEGIN
	
	SET NOCOUNT ON;
	if(@mode='TypeOfTax')--TypeOfTax
	begin

		SELECT [id]
		,[TaxName]
		,[TaxPersentage]
		,[TaxValueInINR]
		,[Active]
		FROM mhc..tbl_Api_TaxDefinition
		WHERE isnull(Active,0) = 1 --Table 0	

		SELECT Id
			,CardName
			,Price
			,BasePrice
		FROM CardType
		WHERE Isnull(isactive,0) = 1
			AND Id = @CardTypeId 
	end
    
END
