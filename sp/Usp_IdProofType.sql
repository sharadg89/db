USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_IdProofType]    Script Date: 09-05-2017 17:53:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 09-May-2017
-- Description:	Id Proof Type Details
-- Updated Date:
-- Updated By:
-- Updated Reason:
-- =============================================
ALTER PROCEDURE [dbo].[Usp_IdProofType]
(@mode varchar(100)
,@IdProofImage nvarchar(500)=null
,@IdProofType int=null
,@OtherType varchar(50)=null
,@IdProofTypeNumber varchar(50)=null
,@DependentId int=null
)
AS
BEGIN
	
	SET NOCOUNT ON;
	if @mode='IDProofType'
	begin
	SELECT Id,IdProof
		FROM IdProofType  where isnull(IsActive,0)=1
	end
	if @mode='InsIdProofType'
	begin
	if not exists(select Id from UserDetails  where Id=@DependentId)
	begin
	Select 'Invalid dependent id';
	return;
	end

	Update UserDetails   set IdProofImage=@IdProofImage,IdProofType=@IdProofType,@OtherType=OtherType,IdProofTypeNumber=@IdProofTypeNumber  where Id=@DependentId
	if @@ROWCOUNT>0
	begin
	Select 'Id proof uploaded successfully' as SMessage
	end
	end
END
