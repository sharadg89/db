-- ================================================
-- @Copyright Vipul Medcare Pvt. Ltd.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE Usp_Transaction
(@mode varchar(100)
,@UserName varchar(100)=null
,@UserId int=null)
as
BEGIN
	
	SET NOCOUNT ON;
	if(@mode='Transaction')--Trisection
	begin

	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		DECLARE @ProviderLogoOPD NVARCHAR(900);
		SET @ProviderLogoOPD = 'http://h3u.com/API/V1/Spine/components/imgs/prov_logo.png';

		SELECT *
		FROM (
			SELECT
				CONVERT(VARCHAR(50), Id) AS [TransactionID]
				,REPLACE(CONVERT(VARCHAR(12), AppoinmentDateTime, 106), ' ', '-') AS [TransactionDate]
				,'Health Checkup' AS [TransactionType]
				,Replace(isnull(UDD.FirstName, ' ') + ' ' + isnull(UDD.MidName, ' ') + ' ' + ISNULL(UDD.LastName, ' '), '  ', ' ') AS [PatientName]
				,c.comm_name AS [ProviderName]
				,'' AS [ProviderCode]
				,HCU.AppoinmentStatus AS [Status]
				,Replace(isnull(UDD.FirstName,'') + ' '+isnull(UDD.MidName,' ')+ ' '+isnull(UDD.LastName,''),'  ',' ') AS [CardHolderName]
				,CM.CardNumber AS [CardNo]
				,'' AS [CouponCode]
				,'' AS [TotalOPDAmount]
				,isnull(a.imagepath,'') AS [ProviderLogo]
			FROM HealthCheckupDtl AS HCU
			JOIN UserDeatils AS UDD ON UDD.Id = HCU.RedDependentId
			JOIN CardMaster AS CM ON CM.Id = HCU.RefCardId
			left join mhc..tbl_provider_outletdetails c on c.id=HCU.RefOutletId
			left join mhc..tbl_ProviderMaster a on a.ProviderCode=c.ProviderCode
			WHERE HCU.RefLoginId = @UserId
			
			UNION
			
			SELECT CONVERT(VARCHAR(50), PARSENAME(REPLACE(UTD.barcode_url, '~/BarCode/', '.'), 2)) AS [TransactionID]
				,REPLACE(CONVERT(VARCHAR(12), status_date, 106), ' ', '-') AS [TransactionDate]
				,'OPD' AS [TransactionType]
				,Replace(isnull(DD.FirstName, '') + ' ' + isnull(DD.MidName, '') + ' ' + ISNULL(DD.LastName, ''), '  ', ' ') AS [PatientName]
				,dbo.ToTitleCase(upper(p.brand_name)) AS [ProviderName]
				,tl.usercode AS [ProviderCode]
				,UTD.chk_status AS [Status]
				--,UTD.barcode_url
				,CM.card_name AS [CardHolderName]
				,UTD.CardNo AS [CardNo]
				,UTD.CouponCode AS [CouponCode]
				,OpdAmount AS [TotalOPDAmount]
				,(
					CASE 
						WHEN ISNULL(p.prov_logo, '') = ''
							THEN @ProviderLogoOPD
						ELSE ISNULL('http://h3u.com/API/V1/Spine/prov_uploadlogo/' + p.prov_logo, '')
						END
					) AS [ProviderLogo]
			FROM MHC..UserCardTransactionDtl AS UTD
			JOIN CardMaster AS CM ON CM.CardNumber = UTD.CardNo
			JOIN UserDeatils AS DD ON DD.Id = UTD.KYCUserID
			LEFT JOIN MHC..tbl_login AS lo ON lo.id = UTD.loginId
			LEFT JOIN MHC..tbl_ProviderMaster AS p ON p.ProviderCode = lo.usercode
			Join CardCoupon as CMC ON CMC.InsertedFrom=dd.Id
			WHERE	CMC.InsertedBy=@UserId
			) dev 
	end
END
GO
