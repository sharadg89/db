USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_HealthCheckUp]    Script Date: 12-05-2017 10:13:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Usp_HealthCheckUp]
(@mode varchar(100)
,@Userid int=null
,@UserName varchar(100)=null
,@CardNumber varchar(16)=null
,@CardId int=null
,@StateId int=null
,@State int=null
,@CityId int = null
,@City int=null
,@DependentId int= null
,@PackageId int =NULL
,@OutletId int =NULL
,@InsertedBy int = null
,@RefLoginId int = null
,@AppTokenId varchar(200) NULL
,@AppoinmentDateTime datetime= NULL
,@AppoinmentStatus varchar(50) =NULL
,@Callstatus varchar(50)= NULL
,@CancelledOnReschedule varchar(50)= NULL
,@Hour varchar(2)= NULL
,@Miniute varchar(3)= NULL
,@PreferredProvider nvarchar(50)= NULL
,@ReportLink nvarchar(500) =NULL
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @mode='CheckUpRequest'--CheckUpRequest
	begin
	    
		BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY

	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		Select  @CardId=Dbo.GetCardId(@CardNumber) 
		if(@CardId=0)
		begin
		select 'Invalid Card';
		ROLLBACK;
		return;
		end

		Select  @State=Dbo.GetStateId(@StateId) 
		if(@State=0)
		begin
		select 'Invalid State';
		ROLLBACK;
		return;
		end

		Select  @City=Dbo.CheckCityState(@StateId,@CityId) 
		if(@City=0)
		begin
		select 'Invalid City';
		ROLLBACK;
		return;
		end

		DECLARE @BasicCheckup INT;

		SELECT @BasicCheckup = CDT.BasicCheckup
		from CardMaster as CD
		join CardType as CDT on CDT.Id=CD.RefCardTypeId
		where CD.Id=@CardId

		
		IF (@BasicCheckup > 0)
		BEGIN
		IF ((SELECT COUNT(Id) FROM HealthCheckupDtl WHERE RefCardId = @CardId) >= @BasicCheckup )
		BEGIN
		SELECT 'You already used maximum card limit HealthCheckUP' AS [Message]
		ROLLBACK;
		RETURN;
		END
		END
		else
		begin
		SELECT 'Health checkup are not allowed for this card' AS [Message]
		ROLLBACK;
		RETURN;
		end

		set @AppTokenId='TOKEN0'+convert(varchar(50),(sELECT MAX(HealthBookId)+1 FROM H3U_Corp_HealthCheckDtl))

        INSERT INTO [dbo].[HealthCheckupDtl]
		    ([RefDependentId],[RefPackageId],[RefCityId],[RefStateId],[RefOutletId],[InsertedBy],[RefLoginId],[AppTokenId]
			,[AppoinmentDateTime],[AppoinmentStatus],[Callstatus],[RefCardId],[Hour],[Miniute],[PreferredProvider])VALUES

		    (@DependentId,@PackageId,@CityId,@StateId,@OutletId,@InsertedBy,@RefLoginId,@AppTokenId
			,@AppoinmentDateTime,'Appointment','Appointment Fixed',@CardId,@Hour,@Miniute,@PreferredProvider)

		IF @@ROWCOUNT > 0
		BEGIN
		SELECT 'your appointment for checkup booked successfully' AS [Message]

				,REPLACE(CONVERT(VARCHAR(12), @AppoinmentDateTime, 106), ' ', '-') AS [CheckUPDate]
				,@Hour as [Hour]
				,@Miniute as [Miniute]
				,@AppTokenId as [BookingRefId]
			FROM H3U_Corp_HealthCheckDtl
		END
		ELSE
		BEGIN
			SELECT 'Health checkup booking failed' AS [Message]
		END
           
        

		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		exec usp_InserErrorLog
			SELECT 'Please contact to administrator' AS [Message]
		END	CATCH
	END

	IF @mode ='UpdateHealthCheckup'--UpdateHealthCheckupRequest
	BEGIN

	    BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY


	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		IF NOT EXISTS (SELECT AppTokenId FROM HealthCheckUpDtl WHERE AppTokenId = @AppTokenId)
		BEGIN
			SELECT 'Invalid booking refrence id' AS [Message]
			ROLLBACK;
			RETURN;
		END

		

		UPDATE HealthCheckUpDtl SET PreferredProvider=@PreferredProvider ,RefLoginId=@UserId,UpdateOn=GETDATE() where AppTokenId=@AppTokenId
		if(@@ROWCOUNT>0)
		begin
		SELECT 'your preferred provider list is received and successfully updated gains booking reference id' as [Success]
		RETURN;
		end

		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		exec usp_InserErrorLog
			SELECT 'Please contact to administrator' AS [Message]
		END	CATCH
		
	END
END
