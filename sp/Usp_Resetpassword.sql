USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_Resetpassword]    Script Date: 11-05-2017 10:13:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Sharad Gupta
-- Create date: 
-- Description:	set temporary password
-- =============================================
ALTER PROC [dbo].[Usp_Resetpassword] (
	 @UserName VARCHAR(100) = NULL
	,@mode VARCHAR(100) = NULL
    ,@UserId int =null
	,@tomailid VARCHAR(100) = NULL
	)
AS
BEGIN
	--set temporary password
	IF @mode = 'ResetPasswordForMail'
	BEGIN
	   Select  @UserId=Dbo.getUserId(@UserName)
	   If(@UserId=0)
	   begin
	   SELECT 'false' AS [success]
				,'Invalid user' AS [Message]
				return;
	   end

	   select   @tomailid =EmailId from UserDetails where RefLoginId=@UserId

	   if(isnull(@tomailid,'')='')
	   begin
	    SELECT 'false' AS [success]
				,'Please contact to administrator' AS [Message]
				return;
	   end
		DECLARE @r VARCHAR(7)

		SELECT @r = coalesce(@r, '') + CHAR(CASE 
					WHEN r BETWEEN 0
							AND 9
						THEN 48
					WHEN r BETWEEN 10
							AND 35
						THEN 55
					ELSE 61
					END + r)
		FROM master..spt_values
		CROSS JOIN (
			SELECT CAST(RAND(ABS(CHECKSUM(NEWID()))) * 61 AS INT) r
			) a
		WHERE type = 'P'
			AND number < 8

		-------------------------------generate random password--------------------------------------------------------------
		UPDATE LoginAccount
		SET isTempPw = 1
			,pwd = @r
		WHERE Id = @UserId

		IF @@rowcount > 0
		BEGIN
			DECLARE @temppassword VARCHAR(500);

			SET @temppassword = 'Your temporary password is ' + @r;

			EXEC h3u..Mail_AutoSend 'info@h3u.com'
				,@tomailid
				,''
				,'Your temporary password alert'
				,@temppassword

			SELECT 'true' AS [success]
				,'Temporary password generate successfully' AS [message]
		END
		ELSE
		BEGIN
			SELECT 'false' AS [success]
				,'Temporary password not generate successfully' AS [message]
		END
	END --set temporary password
END
