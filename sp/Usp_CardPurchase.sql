-- ================================================
-- @Copyright Vipul MedCare Pvt. Ltd. 2017-18
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 08-May-2017
-- Description:	
-- =============================================
alter PROCEDURE Usp_CardPurchase
(@mode varchar(100)
,@Id int = null
,@UserName varchar(100)=null
,@CardTypeId int = null
,@StateId int =null
,@CityId int =null
,@DependentId int =null
,@DOB date=null
,@DiscountCode varchar(25)=null
,@MID NVARCHAR(200)=NULL
,@TXNID nvarchar(200)=NULL
,@ORDERID  nvarchar(200)=NULL
,@BANKTXNID nvarchar(200)=NULL
,@TXNAMOUNT  decimal(18,2)=NULL
,@CURRENCY NCHAR(3) = NULL
,@STATUS  nvarchar(200)=NULL
,@RESPCODE  nvarchar(200)=NULL
,@RESPMSG nvarchar(200)=NULL
,@TXNDATE varchar(100)=NULL
,@GATEWAYNAME  nvarchar(200)=NULL
,@BANKNAME  nvarchar(200)=NULL
,@PAYMENTMODE  nvarchar(200)=NULL
,@CHECKSUMVALID nvarchar(500)=NULL
,@TXNTYPE  nvarchar(200)=NULL
,@REFUNDAMT  nvarchar(50)=NULL
,@AmountPayable decimal(18,2)=NULL 
,@DATA nvarchar(MAX)=NULL
,@CardNumber varchar(16)=null
,@DsaCode nvarchar(40)=null
,@CreatedOn datetime=null
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    If @mode='BasicDtl'
	Begin

	select @Id=Id  from LoginAccount where UserName=@UserName
	if(isnull(@Id,0)=0)
	begin
	SELECT 'Invalid User' AS [Message]
	END

	IF NOT EXISTS (
	SELECT Id
	FROM CardType
	WHERE Id = @CardTypeId
	)
	BEGIN
	SELECT 'Invalid card type id' AS [Message]
	RETURN;
	END

	IF NOT EXISTS (SELECT Id FROM StateMaster WHERE Id = @StateId)
	BEGIN
	SELECT 'Invalid state' AS [Message]
	RETURN;
	END

	IF NOT EXISTS (
				SELECT Id
				FROM CityMaster
				WHERE RefStateId = @StateId
				)
		BEGIN
			SELECT 'No city exists corresponding to selected state' AS [Message]
			return;
		END


		 DECLARE @DisAmt as nvarchar(100);
		 DECLARE @OriAmt as nvarchar(100);

		--Discount Code validation
		if (isnull(@DiscountCode,'')!='')
		begin
		if not exists(select Id from CardDiscount  where DiscountCode=@DiscountCode)
		begin
		 SELECT 'Invalid promo code' AS [Message];
		 return;
		end

		 SELECT @DisAmt=Isnull(DiscountAmt ,0) FROM CardDiscount WHERE DiscountCode = @DiscountCode   
		 SELECT @OriAmt=Isnull(Price,0) FROM CardType Where Id=@CardTypeId
           
		end--Discount Code validation


		--If discount amount not equal to original amount then must required payment getway suceess response handling
		If (@DisAmt<>@OriAmt)
		BEGIN
		INSERT INTO h3u..H3U_Paytm_PaymentDtl
		    ([SUBS_ID],[MID],[TXNID],[ORDERID],[BANKTXNID],[TXNAMOUNT],[CURRENCY],[STATUS]
           ,[RESPCODE],[RESPMSG],[TXNDATE],[GATEWAYNAME],[BANKNAME],[PAYMENTMODE],[PROMO_CAMP_ID]
		   ,[PROMO_STATUS],[PROMO_RESPCODE],[CHECKSUMHASH],[CreatedOn],[TXNTYPE],[REFUNDAMT],AmountPayable)
		   VALUES
		   ('',@MID,@TXNID,@ORDERID,@BANKTXNID,@TXNAMOUNT,@CURRENCY,@STATUS
           ,@RESPCODE,@RESPMSG,@TXNDATE,@GATEWAYNAME,@BANKNAME,@PAYMENTMODE,''
		   ,'','',@CHECKSUMVALID,@CreatedOn,@TXNTYPE,@REFUNDAMT,@AmountPayable)

		if CHARINDEX('TXN_FAILURE',@STATUS) > 0
		begin
		SELECT 'Your transaction failed' AS [Message]
		RETURN;
		end

		IF (@TXNAMOUNT <> @AmountPayable)
		BEGIN
		SELECT 'Payable amount are not same' AS [Message]
		RETURN;
		END

		END--End If discount amount not equal to original amount then must required payment getway suceess response handling

		BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY

		declare @Success int =0;

		update UserDetails  set StateCode =@StateId,CityCode=@CityId,DOB=@DOB  where RefLoginId=@Id
		if CHARINDEX('TXN_SUCCESS',@STATUS) > 0
		begin
		set @Success=1;
		END
		else if (@OriAmt=@TXNAMOUNT)
		begin
		set @Success=1;
		end

		if(@Success=1)
		begin
		    
			declare @maxid int;
			set   @maxid =(select  top 1 Id  from CardMaster)
		    declare @countlen varchar(20)='';
			declare @lenvaiable int;
			set @countlen = cast(isnull(@CardTypeId,0) as varchar(5))+ cast(isnull(@StateId,0) as varchar(2))+cast(isnull(@CityId,0) as varchar(10))
			set @lenvaiable=16- len(@countlen);
			set @CardNumber= @countlen+ISNULL(Right('000000000000000'+convert(varchar,CAST(1
            AS INT)+1),@lenvaiable),'00000000001')

			if(@TXNAMOUNT <> @AmountPayable)
			BEGIN
			SELECT 'Payable amount are not same' AS [Message]
			ROLLBACK TRANSACTION
			RETURN;
			END
		

			if (upper(isnull(@STATUS,'')) not in ('TXN_SUCCESS','TOTALDISCOUNT' ))
			begin
			SELECT 'your transaction failed' AS [Message]
			ROLLBACK TRANSACTION
			RETURN;
			END

			if(isnull(@DiscountCode,'')!='')
			begin
			UPDATE CardDiscount SET IsActive =0 WHERE DiscountCode = @DiscountCode
			end

			insert into CardMaster
			                      (RefCardTypeId,RefLoginId,CardNumber,CardStatus,DsaCode,DiscountCode)values
								  (@CardTypeId,@Id,@CardNumber,'Pending',@DsaCode,@DiscountCode)
			if @@rowcount>0
			begin
			Select 'Card generated successfully;'
			end
			           

		end

	    COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			SELECT 'Please contact to administrator' AS [Message]
			RETURN;
			ROLLBACK TRANSACTION
		END CATCH


	END
END

