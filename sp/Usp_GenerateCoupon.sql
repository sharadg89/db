-- ================================================
--@CopyRight Vipul Med Care 2017-18
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 07-May-2017
-- Description:	<Description,,>
-- =============================================
alter PROCEDURE Usp_GenerateCoupon
(@mode varchar(100)
,@UserName varchar(100)=''
,@CardNumber varchar(16)=''
,@Id int =Null
,@RefLoginId int =null
,@InsertedFrom int = null
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF @mode ='GenOpdCoupon'
	BEGIN


	            select @RefLoginId=Id   from LoginAccount WHERE UserName = @UserName
				IF isnull(@RefLoginId,0)=0
				BEGIN
				SELECT 'Invalid User' AS [Message]
				RETURN;
				END

				Select @Id=Id  from CardMaster where  CardNumber = @CardNumber

				IF NOT EXISTS (
				SELECT CardNumber
				FROM CardMaster
				WHERE Id=@Id
				AND isnull(IsActive, 0) = 1
				)
				BEGIN
				SELECT 'Invalid Card Number' AS [Message]
				RETURN;
				END

		BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY

				DECLARE @usedopd INT;
				DECLARE @activecoupon INT;

				SELECT @usedopd = COUNT(Id)
				FROM UserCardTransactionDtl
				WHERE RefCardId=@Id                              
				AND ISNULL(IsActive, 0) = 1
				AND ((UPPER(ChkStatus) = 'CLOSED'))
				AND BookType = 'Coupon USE'

				SELECT @activecoupon = COUNT(CDC.Id)
				FROM CardCoupon  as CDC
				Join UserCardTransactionDtl as CDT on CDT.RefCouponId=CDC.Id
				WHERE CDC.RefCardId=@Id
				AND isnull(CDC.IsActive, 0) = 1
				AND datediff(HOUR, (CDC.InsertedOn), getdate()) < 10
				AND UPPER(CDT.ChkStatus) != 'CLOSED'
				AND REPLACE(CONVERT(VARCHAR(12), dateadd(HOUR, 1, CDT.InsertedOn), 106), ' ', '-') + ' ' + CONVERT(VARCHAR(15), CAST(dateadd(HOUR, 1, CDT.InsertedOn) AS TIME), 100) <= REPLACE(CONVERT(VARCHAR(12), GETDATE(), 106), ' ', '-') + ' ' + CONVERT(VARCHAR(15), CAST(GETDATE() AS TIME), 100)
				AND CDT.RefCardId=@Id
				

				DECLARE @opdAveleble INT;
				DECLARE @Couponcnt VARCHAR(10);
				SELECT @opdAveleble = (CARDTYPE.OpdCoupon - (@activecoupon + @usedopd)),@Couponcnt=CARDTYPE.OpdCoupon
				FROM CardMaster AS CARDMST
				JOIN CardType AS CARDTYPE ON CARDTYPE.Id = CARDMST.RefCardTypeId
				WHERE CARDMST.CardNumber = @CardNumber 

				IF (@opdAveleble < 0)
				BEGIN
				SET @opdAveleble = 0;
				SELECT 'No available OPD coupon' AS [Message]
				ROLLBACK TRANSACTION
				RETURN;
				END

				DECLARE @Random VARCHAR(10)
				SET @Random = CONVERT(VARCHAR(6), right(newid(), 10))


				INSERT INTO CardCoupon(
				RefCardId
				,CouponCount
				,CouponCode
				,InsertedBy
				,InsertedFrom
				)
				VALUES (
				@Id
				,@Couponcnt
				,@Random
				,@RefLoginId
				,@InsertedFrom
				)

				DECLARE @num_hours INT=10;

				SELECT
				Replace(UD.FirstName + ' ' + ISNULL(UD.MidName, '') + ' ' + ISNULL(UD.LastName, ''), '  ', ' ') AS [CardName]
				,CDC.CouponCode
				,REPLACE(CONVERT(VARCHAR(12), GETDATE(), 106), ' ', '-') + ' ' + CONVERT(VARCHAR(15), CAST(GETDATE() AS TIME), 100) AS [GenerateOn]
				,REPLACE(CONVERT(VARCHAR(12), dateadd(HOUR, 1, GETDATE()), 106), ' ', '-') + ' ' + CONVERT(VARCHAR(15), CAST(dateadd(HOUR, 1, GETDATE()) AS TIME), 100) AS [ExpireOn]
				FROM CardMaster CD
				INNER JOIN CardCoupon CDC ON CDC.RefCardId = CD.Id
				INNER JOIN UserDetails UD ON UD.ID = CD.RefLoginId
				WHERE CDC.RefCardId = @Id
				AND CDC.CouponCode = @Random
				AND isnull(CDC.IsActive,0) = 1
				AND UD.Id = @InsertedFrom

		COMMIT TRANSACTION
		END TRY

		BEGIN CATCH
			SELECT 'Please contact to administrator' AS [Message]
			RETURN;
			ROLLBACK TRANSACTION
		END CATCH


	END
    
END

