-- ================================================
-- @Copyright Vipul Medcare Pvt. Ltd.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Usp_UsedCoupons
(@mode varchar(100)
,@CardId int=null
,@UserName varchar(100)=null
,@CardNumber varchar(16)=null
,@UserId int=null)
AS
BEGIN
	
	SET NOCOUNT ON;
    if @mode='UsedCouponsDetails'--UsedCouponsDetails
	begin

	    BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY

	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		Select  @CardId=Dbo.GetCardId(@CardNumber) 
		if(@CardId=0)
		begin
		select 'Invalid Card';
		ROLLBACK;
		return;
		end

		SELECT
		CT.CardName
		,CM.CardNumber
		,tcc.CouponCode
		,REPLACE(CONVERT(VARCHAR(12), tcc.InsertedOn, 106), ' ', '-') + ' ' + CONVERT(VARCHAR(15), CAST(tcc.InsertedOn AS TIME), 100) AS [GeneratedOn]
		,REPLACE(CONVERT(VARCHAR(12), dateadd(HOUR, 1, tcc.InsertedOn), 106), ' ', '-') + ' ' + CONVERT(VARCHAR(15), CAST(dateadd(HOUR, 1, tcc.InsertedOn) AS TIME), 100) AS [ExpiredOn]
		,Replace((isnull(UD.FirstName, '') + ' ' + isnull(UD.MidName, '') + ' ' + ISNULL(UD.LastName, '')), '  ', ' ') [GeneratedBy]
		,UTD.StatusDate AS [UsedDate]
		FROM UserCardTransactionDtl AS UTD
		JOIN CardMaster AS CM ON UTD.RefCardId = CM.ID
		JOIN CardType AS CT ON CT.Id = CM.RefCardTypeId
		JOIN CardCoupon AS tcc ON tcc.RefCardId = UTD.RefCardId
		JOIN UserDetails as UD  on UD.RefLoginId=CM.RefLoginId
		WHERE UTD.RefCardId = @CardId
		AND UTD.BookType = 'Coupon USE'
		AND UTD.ChkStatus = 'Closed'


		COMMIT TRANSACTION
		END TRY
		BEGIN CATCH
		exec usp_InserErrorLog
			SELECT 'Please contact to administrator' AS [Message]
		END	CATCH

	end
END
GO
