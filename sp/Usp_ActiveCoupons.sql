-- ================================================
-- @Copyright Vipul Medcare Pvt. Ltd. 2017-18
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	
-- =============================================
CREATE PROCEDURE Usp_ActiveCoupons
(@mode varchar(100)
,@CardId int=null
,@UserName varchar(100)=null
,@CardNumber varchar(16)=null
,@UserId int=null)

AS
BEGIN
	
	SET NOCOUNT ON;

    if @mode='ActiveCouponsDetails'  --ActiveCouponsDetails
	begin

	 BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY

	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		Select  @CardId=Dbo.GetCardId(@CardNumber) 
		if(@CardId=0)
		begin
		select 'Invalid Card';
		ROLLBACK;
		return;
		end

		SELECT 
		CD.CardNumber
		,CDC.CouponCode
		FROM CardMaster as CD
		Join CardCoupon as CDC on CDC.RefCardId=CD.Id
		WHERE isnull(CD.IsActive,0) = 1
		AND CD.Id = @CardId


		END TRY
		BEGIN CATCH
		exec usp_InserErrorLog
			SELECT 'Please contact to administrator' AS [Message]
		END	CATCH

	end
END
GO
