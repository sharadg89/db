USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_ActiveCoupons]    Script Date: 11-05-2017 12:55:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Usp_ActiveCoupons]
(@mode varchar(100)
,@CardId int=null
,@UserName varchar(100)=null
,@CardNumber varchar(16)=null
,@UserId int=null)

AS
BEGIN
	
	SET NOCOUNT ON;

    if @mode='ActiveCouponsDetails'  --ActiveCouponsDetails
	begin

	 BEGIN TRANSACTION SERIALIZABLE
		BEGIN TRY

	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		Select  @CardId=Dbo.GetCardId(@CardNumber) 
		if(@CardId=0)
		begin
		select 'Invalid Card';
		ROLLBACK;
		return;
		end

		declare @GeneratedBy varchar(300)=''
		select @GeneratedBy=  Replace((isnull(UD.FirstName, '') + ' ' + isnull(UD.MidName, '') + ' ' + ISNULL(UD.LastName, '')), '  ', ' ') from UserDetails  as UD
		Join CardMaster as CD on CD.RefLoginId= UD.RefLoginId
		where CD.Id=@CardId

			SELECT
			@GeneratedBy AS [GeneratedBy]
			,@GeneratedBy AS [CardHolderName]
			--,Replace((isnull(UDD.DependentFName, '') + ' ' + isnull(UDD.DependentMName, '') + ' ' + ISNULL(UDD.DependentLName, '')), '  ', ' ') AS [GeneratedFor]
			,Replace((isnull(UDD.FirstName, '') + ' ' + isnull(UDD.MidName, '') + ' ' + ISNULL(UDD.LastName, '')), '  ', ' ') AS [GeneratedFor]
			,(case when isnull(RM.Id,'') ='' then 'Self' else  RM.Relation end)  RelationShip
			,TCM.CardNumber
			,CGen.CouponCode
			,REPLACE(CONVERT(VARCHAR(12), CGen.InsertedOn, 106), ' ', '-') + ' ' + CONVERT(VARCHAR(15), CAST(CGen.InsertedOn AS TIME), 100) AS [CreatedDate]
			,REPLACE(CONVERT(VARCHAR(12), dateadd(HOUR, 1, CGen.InsertedOn), 106), ' ', '-') + ' ' + CONVERT(VARCHAR(15), CAST(dateadd(HOUR, 1, CGen.InsertedOn) AS TIME), 100) AS [ExpireDate]
			,'Active' AS STATUS
			FROM CardCoupon AS CGen
			JOIN UserDetails AS UDD ON UDD.Id = CGen.InsertedFrom
			JOIN CardMaster AS TCM ON TCM.Id = CGen.RefCardId
			left join UserDependentDetails as DLnk on DLnk.RefDependentId=UDD.Id
			left join Relations as RM on RM.ID=DLnk.RefRelationId
			WHERE
			isnull(CGen.IsActive, 0) = 1
			--AND datediff(HOUR, (CGen.fcdt), getdate()) < 10
			AND CGen.RefCardId = @CardId -- '1162000000000011' --
			-- updated by sharad gupta on 12-12-2016
			AND CGen.CouponCode NOT IN (
			SELECT CouponCode
			FROM MHC..UserCardTransactionDtl
			WHERE cardno = @CardNumber
			AND ((UPPER(chk_status) = 'CLOSED'))
			AND BookType = 'Coupon USE'
			)
			AND DATEADD(HH, 1, CGen.InsertedOn) >= GETDATE()




		END TRY
		BEGIN CATCH
		exec usp_InserErrorLog
			SELECT 'Please contact to administrator' AS [Message]
		END	CATCH

	end
END
