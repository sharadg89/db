-- ================================================
-- @Copyright Vipul Medcorp Pvt. Ltd. 2017-18
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 10-May-2017
-- Description:	Relation
-- =============================================
CREATE PROCEDURE Usp_Relation
(
@mode varchar(100)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	if @mode='Relation'
	begin
	select Id,Relation from Relations  where isnull(IsActive,0)=1
	end
END
GO
