-- ================================================
-- @Copyright Vipul Med Care Pvt. Ltd.  2017-18
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 08-May-2016
-- Description:	State City Details
-- =============================================
CREATE PROCEDURE Usp_State_City
(@mode varchar(100)
,@StateId int = null)
AS
BEGIN
	
	SET NOCOUNT ON;

    if @mode= 'State'
	begin
	
	SELECT Id,StateName
		FROM StateMaster  where ISNULL(IsActive,0)=1
	end

	iF @mode= 'City'
	begin
	IF Not EXISTS (Select Id from CityMaster where RefStateId=@StateId)
	Begin
	SELECT 'Invalid State' AS [Message]
	return;
	End

	SELECT Id, CityName
			FROM CityMaster
			WHERE IsNull(IsActive,0) = 1
				AND RefStateId = @StateId
			ORDER BY CityName
	End
END

