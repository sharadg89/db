USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_Testimonial]    Script Date: 08-05-2017 10:30:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 08-May-2017
-- Description:	Testimonial
-- =============================================
ALTER PROCEDURE [dbo].[Usp_Testimonial]
	-- Add the parameters for the stored procedure here
	(
	@mode varchar(100)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF @mode = 'Testimonial'
	BEGIN
	select Testimonials  from Testimonial  where Isnull(IsActive,0)=1
	END

END
