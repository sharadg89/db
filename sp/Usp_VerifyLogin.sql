USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_VerifyLogin]    Script Date: 10-05-2017 17:44:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[Usp_VerifyLogin] (
@USER_ID INT = NULL
,@UserName varchar(100)=null
,@password  varchar(16)=null
,@mode varchar(50)=null
,@DocOnCall  varchar(50)=null
,@LoginType int=null
,@SocialId varchar(100)=''
,@CorpId int = ''
)
as
begin
	SET NOCOUNT ON;
	

 ----Mode Start  verify login  
     IF @mode = 'VerifyLogin'
	BEGIN
	    
		BEGIN TRY 
	    SELECT @USER_ID=Id
				    FROM [dbo].[LoginAccount]
				    WHERE(
						@LoginType = 1
						AND UserName = @UserName
						AND Pwd = @password COLLATE SQL_Latin1_General_CP1_CS_AS
						)
					OR (
						@LoginType = 2
						AND FbId = @SocialId COLLATE SQL_Latin1_General_CP1_CS_AS
						)
						OR (
						@LoginType = 3
						AND GoogleId = @SocialId COLLATE SQL_Latin1_General_CP1_CS_AS
						)
						OR (
						@LoginType = 4
						AND UserName = @UserName
						AND Pwd = @password COLLATE SQL_Latin1_General_CP1_CS_AS
						AND CorpId=@CorpId
						)
					AND isnull(IsActive,0) = 1

		IF(isnull(@USER_ID,0)!=0)
		BEGIN
		---------------------------------check doc on call and MedHealthCard Aubscripition-----------------------------

		DECLARE @countMedhealthsubscription INT=0;
		select @countMedhealthsubscription= COUNT(SUP.Id),@DocOnCall=count(case when isnull(CD.DocOnCall,0) =0 then 0 else 1 end) from CardMaster as CardMst
							join  LoginAccount as SUP on SUP.Id = CardMst.RefLoginId
							join CardType as CD on CD.Id=CardMst.RefCardTypeId
							where SUP.Id=@USER_ID 

		if(@DocOnCall=0)
		set @DocOnCall= 'YES'
		else
		set @DocOnCall= 'NO'

		----------------------------------------------------------------------------------------------------------------

			

			SELECT 
			UsrAcc.UserName
		   ,Replace(isnull(UsrDtl.FirstName,'') + ' '+isnull(UsrDtl.MidName,'')+' '+isnull(UsrDtl.LastName,''),'  ',' ')as [Name]
		   ,RTRIM(LTRIM(UsrAcc.UserType)) AS UserType
		   ,case when isnull(UsrAcc.UserType,'')='hr' then 'true' else 'false' end IsHR   
		   ,CASE When isnull(UsrDtl.gender,'')='' then 'Notknown'
				 When isnull(UsrDtl.gender,'')=1 then 'Male'
				 When isnull(UsrDtl.gender,'')=2 then 'Female' end Gender
				,CASE @countMedhealthsubscription WHEN 1	THEN '1' ELSE '0' END IsMedhealthcardSubscription
				,'false' as IsProfileVerfied-- added sharad it does no menas and need to discuss
				,case when isnull(UsrDtl.ProfileImgCustom,'')='' then ''
				      else  ProfileImgCustom end as [ProfileImage]
				,@DocOnCall as DocOnCall
				,isnull(UsrDtl.MobileNo,'') as Mobile
				,case when Isnull(IsTempPass,0)=0 then 'true' else 'false' end as [IsTempPwd]
			FROM LoginAccount as UsrAcc
			JOIN UserDetails as UsrDtl on UsrDtl.RefLoginID=UsrAcc.Id
			WHERE UsrAcc.Id = @USER_ID

			select distinct
			FeatureShortName,gp.GrpName
			,Case When t.ID is null Then 'false' else 'true' end  as [active]
			from
			CardFeatureName SM
			left join (
			Select 
			S.ID
			From CardFeatureName S
			Inner join CardFeatureSet F on  S.Id=F.FeatureId
			Inner join CardMaster M on M.Id=F.RefCardTypeId
			and RefLoginId=@USER_ID
			) as t on t.id=SM.ID
			inner join CardFeatureGroup gp on gp.id=sm.featureGroupid
			order by gp.GrpName


		END
		ELSE
			SELECT 'wrong username/password' AS [Message]

		END TRY  
		BEGIN CATCH  
        exec usp_InserErrorLog
		END CATCH;  


	END ----Mode end  verify login  

END