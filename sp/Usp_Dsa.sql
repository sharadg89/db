-- ================================================
-- @Vipul Medcare Pvt. Ltd.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Usp_Dsa
(@mode varchar(100)
,@Userid int=null
,@UserName varchar(100)=null
,@DsaCode varchar(25)=null)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 
	

    if(@mode='CheckDsa')--DSAValidation
	begin

	BEGIN TRANSACTION SERIALIZABLE
	BEGIN TRY

	IF NOT EXISTS (
	SELECT Id
	FROM DsaMaster
	WHERE Dsanumber = @DsaCode
	)
	BEGIN
	SELECT 'DSA coder is invalid' AS [Message]
	ROLLBACK;
	RETURN;
	END
		
	SELECT DsaName as [Name],MobileNo, [EmailId]
	from DsaMaster where DSAnumber=@DsaCode

	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	exec usp_InserErrorLog
	SELECT 'Please contact to administrator' AS [Message]
	END	CATCH

	END
   
END

