-- ================================================
-- @Copyright Vipul Med Care Pvt. Ltd. 2017-18
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Usp_ActiveHealthCheckUp
(@mode varchar(100)
,@UserName varchar(100)=null
,@UserId int=null
,@CardNumber varchar(16)=null
,@CardId int=null)
AS
BEGIN
	SET NOCOUNT ON;

	if(@mode='ActiveHealthCheckUp')--ActiveHealthCheckUPDetail
	begin
	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end

		Select  @CardId=Dbo.GetCardId(@CardNumber) 
		if(@CardId=0)
		begin
		select 'Invalid Card';
		ROLLBACK;
		return;
		end

		declare @dependentId int;
		select @dependentId = Id  from  UserDetails  where RefLoginId=@UserId

		SELECT HealthChk.Id AS [OrderID]
			,REPLACE(CONVERT(VARCHAR(12), HealthChk.InsertedOn, 106), ' ', '-') AS [BookingDate]
			,Replace(isnull(UDD.FirstName, '') + ' ' + isnull(UDD.MidName, '') + ' ' + ISNULL(UDD.LastName, ''),'  ',' ') AS [PatientName]
			,HealthChk.AppoinmentStatus AS [Status]
			,REPLACE(CONVERT(VARCHAR(12), HealthChk.AppoinmentDateTime, 106), ' ', '-') AS [CheckUpDate]
			,(HealthChk.Hour + ':' + HealthChk.Miniute) AS TIME
		FROM HealthCheckupDtl AS HealthChk -- MHC..MHC_HealthCheckUP AS HealthChk
		JOIN UserDetails UDD ON UDD.Id = HealthChk.RefDependentId
		JOIN StateMaster AS SM ON SM.Id = HealthChk.RefStateId
		JOIN CityMaster AS CM ON CM.Id = HealthChk.RefCityId
		JOIN CardMaster AS CardMst ON CardMst.Id = HealthChk.RefCardId
		JOIN CardType AS CardT ON CardT.Id = CardMst.RefCardTypeId
		WHERE HealthChk.RefDependentId = @dependentId
			AND HealthChk.RefCardId = @CardId
	end
END
