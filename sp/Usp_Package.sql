USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_Package]    Script Date: 09-05-2017 18:11:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 08-May-2017
-- Description:	Package Details
-- =============================================
ALTER PROCEDURE [dbo].[Usp_Package] 
(@mode varchar(100)
,@PackID int =null
,@CardNumber varchar(16)=null
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @mode = 'PackageDtl'
	BEGIN
	IF NOT EXISTS (
				SELECT Id
				FROM PackageMaster
				WHERE Id = @PackID
				)
		BEGIN
			SELECT 'Invalid Package Id' AS [Message]
			return;
		END

				SELECT 
				Id
				,PackageName
				FROM PackageMaster
				WHERE Id = @PackID 

				SELECT 
				PD.ProfileName
				,PD.Id As ProfileId
				FROM ProfileDetails as PD
				Join TestMaster as TM on TM.PackageId=PD.Id
				Where TM.PackageId = @PackID

				SELECT TestName
				,Id as TestId
				,ProfileId
				FROM TestMaster
				WHERE PackageId = @PackID 

	END

	if @mode ='HealthPackName'
	begin
    select CardPackage.RefPackageID as PackageID,PackMst.Packagename  from CardTypePackage as CardPackage
	join PackageMaster as PackMst on PackMst.Id=CardPackage.RefPackageID
	where CardPackage.RefCardTypeId in(select RefCardTypeId  from CardMaster  where  CardNumber = @CardNumber)
	end
	
END
