USE [V1_H3U]
GO
/****** Object:  StoredProcedure [dbo].[Usp_NetworkSearch]    Script Date: 12-05-2017 10:11:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Sharad Gupta
-- Create date: 11-May-2017
-- Description:	
-- =============================================
ALTER PROCEDURE [dbo].[Usp_NetworkSearch]
(
@mode varchar(100)
,@SearchBy varchar(200)=null
,@Search varchar(200)=null
,@UserId int=null
,@UserName varchar(100)=null
,@CardId int=null
,@CardNumber varchar(16)=null
,@latitude VARCHAR(35) =null
,@longitude VARCHAR(35) =null
)
AS
BEGIN
	SET NOCOUNT ON;

	if(@mode='NetworkSearch')--NetworkSearch
	begin

	DECLARE @SQLQuery AS NVARCHAR(4000)
	DECLARE @ParamDefinition AS NVARCHAR(2000)

	 
	 SET @SQLQuery = 'SELECT ProviderName,  
      PM.EmailId,  
      PM.PhoneNo,  
      PM.MobileNo,  
      PM.Address,  
      PM.Location,  
      PM.Pincode,  
      SM.StateName,  
      CM.CityName   
            FROM MHC..tbl_ProviderMaster as PM  
                 join tbl_StateMaster as SM  
                 on PM.State=SM.StateCode  
             Join tbl_CityMaster as CM  
                 on CM.CityCode=PM.City where (1=1)'

		IF @SearchBy IS NOT NULL
			AND @SearchBy = 'STATE'
		BEGIN
			SET @SQLQuery = @SQLQuery + ' And StateName like ''%' + @Search + '%'''
				--return;  
		END
		ELSE IF @SearchBy IS NOT NULL
			AND @SearchBy = 'CITY'
		BEGIN
			SET @SQLQuery = @SQLQuery + ' And CityName like ''%' + @Search + '%'''
				--return;  
		END
		ELSE IF @SearchBy IS NOT NULL
			AND @SearchBy = 'PINCODE'
		BEGIN
			SET @SQLQuery = @SQLQuery + ' And Pincode like ''%' + @Search + '%''' 
		END
		ELSE IF @SearchBy IS NOT NULL
			AND @SearchBy = 'LOCATION'
		BEGIN
			SET @SQLQuery = @SQLQuery + ' And Location like ''%' + @Search + '%'''
				--return;  
		END
 
		EXECUTE sp_Executesql @SQLQuery


	end

	if(@mode='GeneralNetwork')--GeneralNetwork_Old
	begin
	    Select  @UserId=Dbo.getUserId(@UserName) 
		if(@Userid=0)
		begin
		select 'Invalid User';
		ROLLBACK;
		return;
		end
			SELECT DISTINCT longitude AS Longitute
			,latitude AS Latitute
			,comm_name AS NAME
			,out_address AS Address
			,mobileno AS MobileNo
			,phoneno AS ContactNo
			,ProviderType
			,'' AS [WebLink]
			,'3.5' AS [Rating]
			,chk_opd AS [OPD]
			,chk_radio AS [Radiology]
			,chk_diag AS [Diagnosis]
			,chk_pharm AS [Pharmacy]
			FROM (
			SELECT [dbo].[fnCalcDistanceKM](@latitude, latitude, @longitude, longitude) AS distance
			,rtrim(ltrim(c.longitude)) as longitude
			,rtrim(ltrim(c.latitude))  as latitude
			,c.comm_name
			,c.out_address
			,c.mobileno
			,c.phoneno
			,IsNULL(PT.ProviderName, 'Hospital') AS ProviderType
			,c.chk_opd
			,c.chk_radio
			,c.chk_diag
			,c.chk_pharm
			FROM MHC..tbl_ProviderMaster a
			inner JOIN MHC..tbl_provider_outletdetails c ON c.ProviderCode = a.ProviderCode
			LEFT JOIN MHC..tbl_ProviderType PT ON PT.ProviderTypeCode = a.ProviderTypecode
			WHERE c.fcby NOT IN (
			''
			,'prov1@h3u.com'
			,'sdf'	
			)
			and c.latitude like '%.%' and c.longitude like '%.%'  and c.id not in (379,438,501,437,669,651,				
			300,301,436,688,3622,3682,6315,6427,6431,6486,6567,6580,6691,6713,6871,7023,7024,7026,7210)
			AND ISNUMERIC(c.latitude) = 1
			AND ISNUMERIC(c.longitude) = 1 and c.latitude not like '00.0%' and c.longitude not like '00.0%'
			) sha
			WHERE sha.distance <= 10 --@distance
			AND sha.distance >= 0

		if (@@rowcount=0)
		begin
		SELECT 'No Record found' AS [Message]
		end
		
	end

	if(@mode='GetAllProvider')--GetAllProvider
	begin
	SELECT b.id AS [id]
			,SM.StateName
			,CM.CityName
			,a.brand_name AS [BrandName]
			,isnull(Ptype.ProviderName, 'Hospital') AS ProviderType
			,SUBSTRING(LTRIM(RTRIM(CASE 
							WHEN ISNULL(b.inhouse_diag, 0) = 1
								THEN 'Diagnostic,'
							ELSE ''
							END + CASE 
							WHEN ISNULL(b.patho_invest, 0) = 1
								THEN 'Pathology,'
							ELSE ''
							END + CASE 
							WHEN ISNULL(b.radio_invest, 0) = 1
								THEN 'Radiology,'
							ELSE ''
							END + CASE 
							WHEN ISNULL(b.pharmacy_inhouse, 0) = 1
								THEN 'Pharmacy,'
							ELSE ''
							END + CASE 
							WHEN ISNULL(c.chk_opd, 0) = 1
								THEN 'OPD,'
							ELSE ''
							END)), 1, NULLIF(LEN(LTRIM(RTRIM(CASE 
									WHEN ISNULL(b.inhouse_diag, 0) = 1
										THEN 'Diagnostic,'
									ELSE ''
									END + CASE 
									WHEN ISNULL(b.patho_invest, 0) = 1
										THEN 'Pathology,'
									ELSE ''
									END + CASE 
									WHEN ISNULL(b.radio_invest, 0) = 1
										THEN 'Radiology,'
									ELSE ''
									END + CASE 
									WHEN ISNULL(b.pharmacy_inhouse, 0) = 1
										THEN 'Pharmacy,'
									ELSE ''
									END + CASE 
									WHEN ISNULL(c.chk_opd, 0) = 1
										THEN 'OPD,'
									ELSE ''
									END))) - 1, - 1)) AS Speciality
		FROM mhc..tbl_ProviderMaster a
		INNER JOIN mhc..tbl_provider_outletdetails c ON c.ProviderCode = a.ProviderCode
		INNER JOIN mhc..tbl_provider_specialistlink b ON b.ProviderCode = c.ProviderCode
			AND b.outlet_id = c.outlet_id
		INNER JOIN MHC..tbl_StateMaster AS SM ON SM.StateCode = c.out_state
		INNER JOIN MHC..tbl_CityMaster AS CM ON CM.CityCode = c.out_city
		JOIN mhc..tbl_ProviderType AS Ptype ON Ptype.ProviderTypeCode = a.ProviderTypecode
			AND b.outlet_id = c.outlet_id
		WHERE c.ProviderCode NOT IN (
				'PRV0867900001'
				,'PRV1292100001'
				,'PRV01100002'
				,'PRV028200001'
				,'PRV028300001'
				,'PRV0429700001'
				,'PRV0641700001'
				,'PRV18167300001'
				,'PRV15164500016'
				,'PRV28255100001'
				)
			AND c.isactive = 1
		--	and lower(Ptype.ProviderName) in (select lower(items) from dbo.Split(@type,','))
		--and c.pl3autho_by is not null
		ORDER BY a.ProviderCode
			,c.outlet_id
	end

END
