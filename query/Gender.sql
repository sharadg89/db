Create Table Gender
(
Id int identity(1,1) primary key,
Gender varchar(20) not null,
InsertedOn datetime default getdate(),
IsActive int default 1
)

insert into Gender(Gender)values('Male'),('Female')