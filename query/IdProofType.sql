

CREATE TABLE IdProofType(
	[Id] [int] IDENTITY(1,1) Primary key,
	[IDProof] [varchar](30) NULL,
	[InsertedOn] [datetime] default getdate(),
	[InsertedBy] int not null,
	IsActive int default 1
) 
insert into IdProofType(IDProof,InsertedBy)
select  id_proof,1   from  MHC..tbl_idprooftypemaster