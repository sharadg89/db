
CREATE TABLE [dbo].[DsaMaster](
	[Id] [int] IDENTITY(1,1) primary key,
	[DsaNumber] [varchar](25) NULL,
	[DsaName] [varchar](50) NULL,
	[FatherHusbandName] [varchar](50) NULL,
	[MobileNo] [varchar](10) NULL,
	[EmailId] [varchar](100) NULL,
	[Address] [nvarchar](50) NULL,
	[PanNo] [varchar](10) NULL,
	[BankAcNo] [varchar](20) NULL,
	[IfscCode] [varchar](15) NULL,
	[Bankname] [varchar](50) NULL,
	[Branch] [varchar](20) NULL,
	[ComissionPersent] [varchar](3) NULL,
	[DsaType] [nvarchar](20) NULL,
	InsertedOn datetime default getdate(),
	[IsActive] [int] default 1)

	
insert into  [DsaMaster]([DsaNumber],[DsaName],[FatherHusbandName],[MobileNo],[EmailId],[Address],[PanNo],
	[BankAcNo],[IfscCode],[Bankname],[Branch],[ComissionPersent],[DsaType])
	select [DsaNumber],[DsaName],[FatherHusbandName],[MobileNo],[EmailId],[Address],[PanNo],
	[BankAcNo],[IFSCcode],[Bankname],[Branch],[ComissionPersent],[DsaType]  from MHC..DSAMaster