CREATE TABLE CardTypePackage(
	[Id] [int] IDENTITY(1,1) primary key,
	[RefPackageID] [int] NOT NULL,
	[RefCardTypeId] int not null,
	InsertedOn datetime default getdate(),
	IsActive int default 1,
	constraint FK_CDT_CDTP_RefCardTypeId foreign key(RefCardTypeId) references CardType(Id)
	,constraint FK_Pm_CDTP_PackageID foreign key(RefCardTypeId) references PackageMaster(Id)
	)


	insert into CardTypePackage(RefPackageID,RefCardTypeId,IsActive)

	select  PackageID,CardTypeCode,Active from  mhc..tbl_CardTypePackage



