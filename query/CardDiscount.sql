CREATE TABLE CardDiscount(
	[Id] [bigint] IDENTITY(1,1) primary key,
	RefCardTypeId int not null,
	[DiscountAmt] [numeric](7, 2) NULL,
	[DiscountPercentage] [numeric](7, 2) NULL,
	[DiscountCode] [nvarchar](50) NULL,
	[IsActive] int default 1,
	IsPrint int default 0,
	[PrintForwdDate] [datetime] NULL,
	[IsGiven] int default 0,
	[Iscount] int default 0,
	[Remarks] [nvarchar](200) NULL,
	RefDependentId int not null,
	[GeneratedDate] [datetime] default getdate(),
	[Givendate] [datetime] NULL,
	constraint FK_UD_CDD_RefDependentId foreign key(RefDependentId) references UserDetails(Id),
	constraint FK_CDT_CDD_RefCardTypeId foreign key(RefCardTypeId) references CardType(Id),
)

